echo "Generating SSL certificates..."

# Create folder for certs
mkdir -p config/certs

# Create SSL certificates
openssl req -nodes -newkey rsa:4096 -new -x509 -keyout config/certs/self-ssl.key -out config/certs/self.cert

echo "...done"