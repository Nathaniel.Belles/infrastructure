# guacamole

This repo combines the [guacamole-server](https://github.com/apache/guacamole-server) and [guacamole-client](https://github.com/apache/guacamole-client) docker containers with a postgresql database 
to make one docker compose file that runs the entire guacamole application. This repo also contains a notifier extension overlay that allows for users to send push notifications using 
[NTFY](https://ntfy.sh/) on special events like guacamole starting/stopping and authentication success/failure. For additional security, this repo also contains SSL certificates for https on the 
main guacamole web gui.

## Instructions

1. From `guacamole/` run `./build.sh client server` to build the custom `client` and `server` versions of guacamole with notifier extension overlay
2. From `guacamole/guacamole-compose` run `./prepare.sh` to create database initializer and SSL certificates
3. Update the following variables in the `guacamole/guacamole-compose/compose.yml`:
    * `services.guacamole.environment.POSTGRESQL_PASSWORD` and `services.postgres.environment.POSTGRES_PASSWORD`
    * `services.guacamole.environment.HTTPS_KEYSTORE_PASS` (same as used in later step)
    * `services.guacamole.environment.NOTIFIER_NTFY_DOMAIN`
    * `services.guacamole.environment.NOTIFIER_NTFY_TOPIC`
    * `services.guacamole.environment.NOTIFIER_GUACAMOLE_URL`
4. Update the following arguments in the `docker run ...` command in the `guacamole/guacamole-compose/prepare.sh` script for generating self-signed certificates
    * `-dname "..."` with domain name info (optional)
    * `-storepass` and `-keypass` (same as used above)
5. From `guacamole/guacamole-compose` run `docker compose up`

## Usage

Note: For session recording, add `/files/record/${HISTORY_UUID}` to the typescript path or recording path and then give the typescript name or recording name a name like `endpoint-typescript` or 
`endpoint-recording` and then check `Automatically create <typescript/recording> path`.

Note: This uses the non-standard root base context ("0.0.0.0/") for hosting guacamole, unlike normal deployments which use the default context ("0.0.0.0/guacamole").
