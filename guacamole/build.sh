#!/bin/bash

# Variables
GUACAMOLE_VERSION=1.6.0

log() {
	local message="$1"
	local length=${#message}
	local fill_string=$(printf "%-${length}s" "" | tr " " "-")

	echo ""
	echo "----$fill_string----"
	echo "--- $message ---"
	echo "----$fill_string----"
	echo ""
}

for clientserver in "$@"; do
	log "Building for $clientserver..."

	# Download official code
	log "Getting official code"
	wget -nv "--output-document=guacamole-$clientserver-official.zip" "https://github.com/apache/guacamole-$clientserver/archive/refs/heads/staging/$GUACAMOLE_VERSION.zip"

	# Unzip official code
	log "Unzipping official code"
	unzip -q "guacamole-$clientserver-official.zip"

	# Clean up downloads
	log "Cleaning up download"
	rm "guacamole-$clientserver-official.zip"

	# Move official code into place
	log "Moving official code into place"
	mv "guacamole-$clientserver-staging-$GUACAMOLE_VERSION" "guacamole-$clientserver-official"

	# Make working and mount folders
	log "Make folders for mounting"
	mkdir "guacamole-$clientserver-working" "guacamole-$clientserver-builder"

	# Apply overlay
	log "Overlay filesystem onto official code"
	sudo mount -t overlay overlay -o "lowerdir=./guacamole-$clientserver-official,upperdir=./guacamole-$clientserver,workdir=./guacamole-$clientserver-working" "./guacamole-$clientserver-builder"

	# Build docker images
	log "Building docker image"
	docker build --no-cache -t "guacamole-$clientserver-$GUACAMOLE_VERSION" -f "guacamole-$clientserver-builder/Dockerfile" "guacamole-$clientserver-builder"

	# Unmount overlay
	log "Unmounting filesystem overlay"
	sudo umount -v "./guacamole-$clientserver-builder"
	sleep 1 # Seems to take a second to properly unmount before deleting temporary files

	# Delete temporary/generated files
	log "Cleaning up temporary files"
	rm -rf "./guacamole-$clientserver-official" "./guacamole-$clientserver-working" "./guacamole-$clientserver-builder"

	log "...done building for $clientserver"
done
exit
