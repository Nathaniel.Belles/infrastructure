package org.apache.guacamole.event;

import org.apache.guacamole.GuacamoleException;

import javax.annotation.Nonnull;
import org.apache.guacamole.net.auth.Identifiable;
import org.apache.guacamole.net.auth.User;
import org.apache.guacamole.net.auth.Credentials;
import org.apache.guacamole.net.auth.UserContext;
import org.apache.guacamole.net.auth.AuthenticatedUser;
import org.apache.guacamole.net.auth.AuthenticationProvider;
import org.apache.guacamole.net.auth.credentials.GuacamoleInsufficientCredentialsException;
import org.apache.guacamole.net.event.ApplicationStartedEvent;
import org.apache.guacamole.net.event.ApplicationShutdownEvent;
import org.apache.guacamole.net.event.AuthenticationSuccessEvent;
import org.apache.guacamole.net.event.AuthenticationFailureEvent;
import org.apache.guacamole.net.event.UserEvent;
import org.apache.guacamole.net.event.listener.Listener;
import org.apache.guacamole.environment.Environment;
import org.apache.guacamole.environment.LocalEnvironment;
import org.apache.guacamole.event.conf.NotifierGuacamoleProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.AuthProvider;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * A Listener that logs authentication success and failure events.
 */
public class Notifier implements Listener {

    private static final Logger logger = LoggerFactory.getLogger(Notifier.class);

    @Override
    public void handleEvent(Object event) throws GuacamoleException {

        // Get the Guacamole server environment
        Environment environment = LocalEnvironment.getInstance();

        // Get enabled property
        Boolean enabled = environment.getProperty(NotifierGuacamoleProperties.NOTIFIER_ENABLED, false);

        // Quit if not enabled
        if (!enabled) {
            // Shouldn't actually end up here but check anyway
            return;
        }

        // Get remaining properties
        String ntfy_domain = environment.getRequiredProperty(
            NotifierGuacamoleProperties.NOTIFIER_NTFY_DOMAIN
        );
        String ntfy_topic = environment.getRequiredProperty(
            NotifierGuacamoleProperties.NOTIFIER_NTFY_TOPIC
        );
        String guacamole_url = environment.getRequiredProperty(
            NotifierGuacamoleProperties.NOTIFIER_GUACAMOLE_URL
        );
        Boolean notif_on_startup = environment.getProperty(NotifierGuacamoleProperties.NOTIFIER_ON_STARTUP, false);
        Boolean notif_on_shutdown = environment.getProperty(NotifierGuacamoleProperties.NOTIFIER_ON_SHUTDOWN, false);
        Boolean notif_on_success = environment.getProperty(NotifierGuacamoleProperties.NOTIFIER_ON_SUCCESS, false);
        Boolean notif_on_resuccess = environment.getProperty(NotifierGuacamoleProperties.NOTIFIER_ON_RESUCCESS, false);
        Boolean notif_on_failure = environment.getProperty(NotifierGuacamoleProperties.NOTIFIER_ON_FAILURE, false);

        // Successful Authentication
        if (event instanceof AuthenticationSuccessEvent) {
            try {
                AuthenticationSuccessEvent asevent = (AuthenticationSuccessEvent) event;
                AuthenticationProvider authProvider = asevent.getAuthenticationProvider();
                Credentials creds = asevent.getCredentials();
                String username = creds.getUsername();
                String remoteAddress = creds.getRemoteAddress();
                String providerName = authProvider.getIdentifier();

                // New authentication
                if (!asevent.isExistingSession()) {
                    logger.info("User \"{}\" successfully authenticated from {}", username, remoteAddress);

                    // Send push notification
                    if (notif_on_success) {
                        // Create URL from parameter
                        URL url = new URL(ntfy_domain + "/" + ntfy_topic);

                        // Open a connection to the URL
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                        // Set the request method to PUT
                        connection.setRequestMethod("PUT");

                        // Set headers
                        connection.setRequestProperty("Content-Type", "text/plain");
                        connection.setRequestProperty("Title", "Successful Authentication Attempt");
                        connection.setRequestProperty("Tags", "unlock");
                        connection.setRequestProperty("Actions", "view, Open Guacamole, " +
                                guacamole_url + ", clear=false");

                        // Enable output
                        connection.setDoOutput(true);

                        // Create data to send
                        String textData = "Successful authentication event for user \"" +
                                username + "\" on " + guacamole_url + " with " + providerName + " method.";

                        // Get the output stream of the connection
                        OutputStream outputStream = connection.getOutputStream();

                        // Write the text data to the output stream
                        outputStream.write(textData.getBytes());

                        // Close the output stream
                        outputStream.close();

                        // Get the response code
                        int responseCode = connection.getResponseCode();

                        // Log enexpected response code
                        if (responseCode != 200) {
                            logger.warn("Unexpected response Code: {}", responseCode);
                        }

                        // Close the connection
                        connection.disconnect();
                    }
                }
                // Re-authentication
                else {
                    logger.info("User \"{}\" successfully re-authenticated their existing "
                            + "session from {}", username, remoteAddress);

                    // Send push notification
                    if (notif_on_resuccess) {
                        // Create URL from parameter
                        URL url = new URL(ntfy_domain + "/" + ntfy_topic);

                        // Open a connection to the URL
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                        // Set the request method to PUT
                        connection.setRequestMethod("PUT");

                        // Set headers
                        connection.setRequestProperty("Content-Type", "text/plain");
                        connection.setRequestProperty("Title", "Successful Re-Authentication Attempt");
                        connection.setRequestProperty("Tags", "unlock");
                        connection.setRequestProperty("Actions", "view, Open Guacamole, " +
                                guacamole_url + ", clear=false");

                        // Enable output
                        connection.setDoOutput(true);

                        // Create data to send
                        String textData = "Successful re-authentication event for user \"" +
                                username + "\" on " + guacamole_url + " with " + providerName + " method.";

                        // Get the output stream of the connection
                        OutputStream outputStream = connection.getOutputStream();

                        // Write the text data to the output stream
                        outputStream.write(textData.getBytes());

                        // Close the output stream
                        outputStream.close();

                        // Get the response code
                        int responseCode = connection.getResponseCode();

                        // Log enexpected response code
                        if (responseCode != 200) {
                            logger.warn("Unexpected response Code: {}", responseCode);
                        }

                        // Close the connection
                        connection.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        // Unsuccessful Authentication
        else if (event instanceof AuthenticationFailureEvent) {
            try {

                AuthenticationFailureEvent afevent = (AuthenticationFailureEvent) event;

                AuthenticationProvider authProvider = afevent.getAuthenticationProvider();

                Credentials creds = afevent.getCredentials();
                String username = creds.getUsername();
                String remoteAddress = creds.getRemoteAddress();
                String providerName = authProvider.getIdentifier();
                String failureMessage = afevent.getFailure() == null ? "unknown error (no specific failure recorded)"
                        : afevent.getFailure().getMessage();

                if (creds.isEmpty()) {
                    logger.debug("Empty authentication attempt (login screen "
                            + "initialization) from {} failed: {}",
                            remoteAddress, failureMessage);
                } else if (username == null || username.isEmpty()) {
                    logger.debug("Anonymous authentication attempt from user \"{}\" failed: {}",
                            remoteAddress, failureMessage);
                } else if (afevent.getFailure() instanceof GuacamoleInsufficientCredentialsException) {
                    if (authProvider != null) {
                        logger.debug("Authentication attempt from {} for user \"{}\" "
                                + "requires additional credentials to continue: {} "
                                + "(requested by \"{}\")", remoteAddress,
                                username, failureMessage, authProvider.getIdentifier());
                    } else {
                        logger.debug("Authentication attempt from {} for user \"{}\" "
                                + "requires additional credentials to continue: {}",
                                remoteAddress, username, failureMessage);
                    }
                } else {
                    if (authProvider != null) {
                        logger.warn("Authentication attempt from {} for user \"{}\" "
                                + "failed: {} (rejected by \"{}\")", remoteAddress,
                                username, failureMessage, authProvider.getIdentifier());

                        // Send push notification
                        if (notif_on_failure) {
                            // Create URL from parameter
                            URL url = new URL(ntfy_domain + "/" + ntfy_topic);

                            // Open a connection to the URL
                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                            // Set the request method to PUT
                            connection.setRequestMethod("PUT");

                            // Set headers
                            connection.setRequestProperty("Content-Type", "text/plain");
                            connection.setRequestProperty("Title", "Failed Authentication Attempt");
                            connection.setRequestProperty("Tags", "lock");
                            connection.setRequestProperty("Actions", "view, Open Guacamole, " +
                                    guacamole_url + ", clear=false");

                            // Enable output
                            connection.setDoOutput(true);

                            // Create data to send
                            String textData = "Failed authentication event for user \"" +
                                    username + "\" on " + guacamole_url + " with " + providerName + " method.";

                            // Get the output stream of the connection
                            OutputStream outputStream = connection.getOutputStream();

                            // Write the text data to the output stream
                            outputStream.write(textData.getBytes());

                            // Close the output stream
                            outputStream.close();

                            // Get the response code
                            int responseCode = connection.getResponseCode();

                            // Log enexpected response code
                            if (responseCode != 200) {
                                logger.warn("Unexpected response Code: {}", responseCode);
                            }

                            // Close the connection
                            connection.disconnect();
                        }
                    } else {
                        logger.warn("Authentication attempt from {} for user \"{}\" "
                                + "failed: {}", remoteAddress, username,
                                failureMessage);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (event instanceof ApplicationStartedEvent) {
            if (notif_on_startup) {
                try {
                    // Create URL from parameter
                    URL url = new URL(ntfy_domain + "/" + ntfy_topic);

                    // Open a connection to the URL
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    // Set the request method to PUT
                    connection.setRequestMethod("PUT");

                    // Set headers
                    connection.setRequestProperty("Content-Type", "text/plain");
                    connection.setRequestProperty("Title", "Guacamole Starting Up");
                    connection.setRequestProperty("Tags", "green_circle");
                    connection.setRequestProperty("Actions", "view, Open Guacamole, " +
                            guacamole_url + ", clear=false");

                    // Enable output
                    connection.setDoOutput(true);

                    // Create data to send
                    String textData = "Guacamole starting up on " + guacamole_url;

                    // Get the output stream of the connection
                    OutputStream outputStream = connection.getOutputStream();

                    // Write the text data to the output stream
                    outputStream.write(textData.getBytes());

                    // Close the output stream
                    outputStream.close();

                    // Get the response code
                    int responseCode = connection.getResponseCode();

                    // Log enexpected response code
                    if (responseCode != 200) {
                        logger.warn("Unexpected response Code: {}", responseCode);
                    }

                    // Close the connection
                    connection.disconnect();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (event instanceof ApplicationShutdownEvent) {
            if (notif_on_shutdown) {
                try {
                    // Create URL from parameter
                    URL url = new URL(ntfy_domain + "/" + ntfy_topic);

                    // Open a connection to the URL
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    // Set the request method to PUT
                    connection.setRequestMethod("PUT");

                    // Set headers
                    connection.setRequestProperty("Content-Type", "text/plain");
                    connection.setRequestProperty("Title", "Guacamole Shutting Down");
                    connection.setRequestProperty("Tags", "red_circle");
                    connection.setRequestProperty("Actions", "view, Open Guacamole, " +
                            guacamole_url + ", clear=false");

                    // Enable output
                    connection.setDoOutput(true);

                    // Create data to send
                    String textData = "Guacamole shutting down on " + guacamole_url;

                    // Get the output stream of the connection
                    OutputStream outputStream = connection.getOutputStream();

                    // Write the text data to the output stream
                    outputStream.write(textData.getBytes());

                    // Close the output stream
                    outputStream.close();

                    // Get the response code
                    int responseCode = connection.getResponseCode();

                    // Log enexpected response code
                    if (responseCode != 200) {
                        logger.warn("Unexpected response Code: {}", responseCode);
                    }

                    // Close the connection
                    connection.disconnect();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
