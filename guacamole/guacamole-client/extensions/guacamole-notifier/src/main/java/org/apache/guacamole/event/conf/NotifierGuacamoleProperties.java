package org.apache.guacamole.event.conf;

import org.apache.guacamole.properties.StringGuacamoleProperty;
import org.apache.guacamole.properties.BooleanGuacamoleProperty;

/**
 * Utility class containing all properties used by the notifier. The properties
 * defined here must be specified within guacamole.properties to configure the
 * notifier extension.
 */
public class NotifierGuacamoleProperties {

    /**
     * This class should not be instantiated.
     */
    private NotifierGuacamoleProperties() {}

    /**
     * To enable notifications
     */
    public static final BooleanGuacamoleProperty NOTIFIER_ENABLED =
        new BooleanGuacamoleProperty() {

        @Override
        public String getName() { return "notifier-enabled"; }

    };

    /**
     * The NTFY domain to PUT request (adjustable for self-hosted instances)
     */
    public static final StringGuacamoleProperty NOTIFIER_NTFY_DOMAIN =
        new StringGuacamoleProperty() {

        @Override
        public String getName() { return "notifier-ntfy-domain"; }

    };

    /**
     * The NTFY topic to append to the domain
     */
    public static final StringGuacamoleProperty NOTIFIER_NTFY_TOPIC =
        new StringGuacamoleProperty() {

        @Override
        public String getName() { return "notifier-ntfy-topic"; }

    };

    /**
     * The guacamole url for the action button in the notification
     */
    public static final StringGuacamoleProperty NOTIFIER_GUACAMOLE_URL =
        new StringGuacamoleProperty() {

        @Override
        public String getName() { return "notifier-guacamole-url"; }

    };

    /**
     * To enable notification on successful authentication
     */
    public static final BooleanGuacamoleProperty NOTIFIER_ON_STARTUP =
        new BooleanGuacamoleProperty() {

        @Override
        public String getName() { return "notifier-on-startup"; }

    };

    /**
     * To enable notification on successful authentication
     */
    public static final BooleanGuacamoleProperty NOTIFIER_ON_SHUTDOWN =
        new BooleanGuacamoleProperty() {

        @Override
        public String getName() { return "notifier-on-shutdown"; }

    };

    /**
     * To enable notification on successful authentication
     */
    public static final BooleanGuacamoleProperty NOTIFIER_ON_SUCCESS =
        new BooleanGuacamoleProperty() {

        @Override
        public String getName() { return "notifier-on-success"; }

    };

    /**
     * To enable notification on successful reauthentication
     */
    public static final BooleanGuacamoleProperty NOTIFIER_ON_RESUCCESS =
        new BooleanGuacamoleProperty() {

        @Override
        public String getName() { return "notifier-on-resuccess"; }

    };

    /**
     * To enable notification on successful reauthentication
     */
    public static final BooleanGuacamoleProperty NOTIFIER_ON_FAILURE =
        new BooleanGuacamoleProperty() {

        @Override
        public String getName() { return "notifier-on-failure"; }

    };
}

