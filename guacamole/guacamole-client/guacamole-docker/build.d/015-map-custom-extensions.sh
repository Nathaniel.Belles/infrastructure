#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

map_extensions() {

    # Read through each provided path/prefix mapping pair
    mkdir -p "$DESTINATION/environment"
    tr . ' ' | while read -r EXT_PATH VAR_PREFIX; do

        # Add mappings only for extensions that were actually built as part of
        # the build process (some extensions, like the RADIUS support, will
        # only be built if specific build arguments are provided)
        if [ -d "$DESTINATION/extensions/$EXT_PATH/" ]; then
            echo "Mapped: $EXT_PATH -> $VAR_PREFIX"
            mkdir -p "$DESTINATION/environment/$VAR_PREFIX/extensions"
            ln -s "$DESTINATION/extensions/$EXT_PATH"/*.jar "$DESTINATION/environment/$VAR_PREFIX/extensions/"
        else
            echo "Skipped: $EXT_PATH (not built)"
        fi

    done

}

map_extensions <<'EOF'
    guacamole-notifier..........................NOTIFIER_
EOF

