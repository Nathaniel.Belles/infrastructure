#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
#


##
## Add SSL certs to tomcat server.xml
##
enable_ssl_certs() {
    # Set default HTTPS port if not specified
    if [ -z "$HTTPS_HTTPS_PORT" ]; then
        export SSL_HTTPS_PORT="8443"
    fi

    if [ -z "$HTTPS_KEYSTORE_FILE" ] || [ -z "$HTTPS_KEYSTORE_PASS" ] || [-z "$HTTPS_KEY_ALIAS" ]; then
        cat <<END
FATAL: Missing required environment variables
-----------------------------------------------------------------------------------
If using the SSL Certificates, you must provide each of the
following environment variables:

    HTTPS_KEYSTORE_FILE               The filepath to the keystore (e.g. "/files/config/certs/keystore.jks").

    HTTPS_KEYSTORE_PASS               The password to authenticate the keystore.

    HTTPS_KEY_ALIAS                   The alias to lookup in the keystore (e.g. "tomcat")

END
        exit 1;
    else
        echo "Using HTTPS SSL Certificates"

        xmlstarlet edit --inplace \
            --subnode '/Server' --type elem -n Connector \
            --insert '/Server/Connector' --type attr -n port -v "$HTTPS_HTTPS_PORT" \
            --insert '/Server/Connector' --type attr -n maxThreads -v "200" \
            --insert '/Server/Connector' --type attr -n maxParameterCount -v "1000" \
            --insert '/Server/Connector' --type attr -n scheme -v "https" \
            --insert '/Server/Connector' --type attr -n secure -v "true" \
            --insert '/Server/Connector' --type attr -n SSLEnabled -v "true" \
            --insert '/Server/Connector' --type attr -n keystoreFile -v "$HTTPS_KEYSTORE_FILE" \
            --insert '/Server/Connector' --type attr -n keystorePass -v "$HTTPS_KEYSTORE_PASS" \
            --insert '/Server/Connector' --type attr -n keyAlias -v "$HTTPS_KEY_ALIAS" \
            --move '/Server/Connector' '/Server/Service' \
            $CATALINA_BASE/conf/server.xml
    fi
}

# Enable SSL certs
if [ "$HTTPS_ENABLED" = "true" ]; then
    enable_ssl_certs
fi

