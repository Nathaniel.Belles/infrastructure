#!/bin/bash

# Variables
GUACAMOLE_VERSION=1.6.0

log() {
	local message="$1"
	local length=${#message}
	local fill_string=$(printf "%-${length}s" "" | tr " " "-")

	echo ""
	echo "----$fill_string----"
	echo "--- $message ---"
	echo "----$fill_string----"
	echo ""
}


log "Creating database initializer"

# Create folder for initial database
mkdir ./init >/dev/null 2>&1

# Make database files executable
chmod -R +x ./init

# Create database initializer
docker run --rm "guacamole-client-$GUACAMOLE_VERSION" /opt/guacamole/bin/initdb.sh --postgresql > ./init/initdb.sql

log "Creating SSL certs"

# Create folder for certs
mkdir -p ./config/certs

# Make folder writeable from within container
chmod 777 ./config/certs

# Make java key store
docker run --rm -v ./config/certs:/files/config/certs "guacamole-client-$GUACAMOLE_VERSION" keytool -genkey -keyalg RSA -noprompt -alias tomcat -dname "CN=localhost, OU=NA, O=NA, L=NA, S=NA, C=NA" -keystore /files/config/certs/self.jks -validity 9999 -storepass changeme -keypass changeme

# Restore folder permissions
chmod 755 ./config/certs

log "Create and change permissions on persistent folders"

# Create folders
mkdir ./record ./drive >/dev/null 2>&1

sudo chmod 777 ./record ./drive

log "done"
