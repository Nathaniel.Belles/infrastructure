# livestream

## Instructions
1. Create a `secrets.json` with the format: 
```
[
	...
	{"name": "<stream_name>", "stream_key": "<stream_key>", "audio_file": "</path/to/audio/file/in/container>", "primary": <true/false>},
	...
]
```
2. Run the python script to generate the docker compose file: `python3 scripts/compose.py`
3. Build the docker containers: `docker compose --profile debug build`
4. Start the docker containers: `docker compose up -d`
