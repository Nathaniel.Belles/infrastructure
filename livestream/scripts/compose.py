import yaml
import json
import os

with open("secrets.json", "r") as file:
	"""
		Expects the following json format:
		[
			...
			{"name": "<stream_name>", "stream_key": "<stream_key>", "audio_file": "</path/to/audio/file>", "primary": <true/false>},
			...
		]
	"""
	streams = json.load(file)

services = {
	"stream_update":
		{
			"build": {"context": ".", "dockerfile": "Dockerfile"},
			"image": "stream_update",
			"restart": "always",
			"container_name": "stream_update",
			"volumes": ["./:/files/"],
		},
	"builder":
		{
			"build": {"context": ".", "dockerfile_inline": "FROM alpine \n RUN apk add ffmpeg"},
			"image": "stream_stream",
			"container_name": "stream_builder",
			"profiles": ["debug"]
		}
}

for stream in streams:
	services[f"stream_{stream['name']}"] = {
		"image": "stream_stream",
		"restart": "always",
		"container_name": f"stream_{stream['name']}",
		"command": f"ffmpeg -nostdin -f concat -safe 0 -re -stream_loop -1 -i /files/data/playlist.txt -re -stream_loop -1 -i {stream['audio_file']} -c copy -f flv {'rtmp://a.rtmp.youtube.com/live2/' if stream['primary'] else 'rtmp://b.rtmp.youtube.com/live2/'}{stream['stream_key']}{'' if stream['primary'] else '?backup=1'}",
		"volumes": ["./:/files/"],
	}

compose_data = {"services": services}

with open("compose.yml", "w") as file:
	yaml.dump(compose_data, file)

os.makedirs(os.path.dirname("logs/"), exist_ok=True)
os.makedirs(os.path.dirname("data/temp/"), exist_ok=True)
