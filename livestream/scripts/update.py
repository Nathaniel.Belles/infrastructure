import os
import json
import argparse
import requests
from datetime import datetime, date, timedelta

import ffmpeg
from PIL import Image, ImageFont, ImageDraw, ImageFilter


class Weather:
    def __init__(self, date, refresh=True):
        # Constants
        self.weather_url = "https://api.weather.gov/gridpoints/MLB/19,62/forecast"
        self.weather_icon_folder = "/files/data/images/weather"
        self.weather_json_path = "/files/data/temp/weather.json"

        self.refresh = refresh
        self.date = date

        # Try to get new data
        if self.refresh is True:
            self.response = requests.get(self.weather_url)
            self.data = json.loads(self.response.text)

            # Check for invalid data
            if "properties" not in self.data:
                print(f"Received invalid weather data: {self.data}\n")
                self.refresh = False

        # Load data from file
        if self.refresh is False:
            print(f"\nLoading from json...")

            # Load from file
            if os.path.exists(self.weather_json_path):
                with open(self.weather_json_path, "r") as file:
                    self.data = json.load(file)

                print(f"Loaded from json: {str(self.data):.175s}...\n")
            else:
                self.valid = False
        # Valid data
        else:
            # print(f"Received valid weather data: {self.data}")

            # Write to file
            with open(self.weather_json_path, "w") as file:
                json.dump(self.data, file, indent=4)

        # Parse data
        self.parse()

        self.valid = True

    def parse(self):
        self.today_temperature = None
        self.today_forecast = None
        self.today_icon_path = None
        self.today_time_period = None
        self.tonight_temperature = None
        self.tonight_forecast = None
        self.tonight_icon_path = None
        self.tonight_time_period = None
        self.tomorrow_temperature = None
        self.tomorrow_forecast = None
        self.tomorrow_icon_path = None
        self.tomorrow_time_period = None

        periods = self.data.get("properties").get("periods", [])

        # Loop through periods
        for index, period in enumerate(periods):
            # Get datetime of period
            period_dt_start = datetime.fromisoformat(period.get("startTime"))
            period_dt_end = datetime.fromisoformat(period.get("endTime"))

            # Render during morning or evening
            if period_dt_start.date() == self.date and not period.get("isDaytime"):
                # Morning render
                if period_dt_end.date() == self.date:
                    # Store "today"
                    if index + 1 < len(periods):
                        self.today_temperature = periods[index + 1].get("temperature")
                        self.today_forecast = periods[index + 1].get("shortForecast")
                        self.today_time_period = f"{datetime.fromisoformat(periods[index + 1].get('startTime'))} to {datetime.fromisoformat(periods[index + 1].get('endTime'))}"

                    # Find "tonight"
                    if index + 2 < len(periods):
                        self.tonight_temperature = periods[index + 2].get("temperature")
                        self.tonight_forecast = periods[index + 2].get("shortForecast")
                        self.tonight_time_period = f"{datetime.fromisoformat(periods[index + 2].get('startTime'))} to {datetime.fromisoformat(periods[index + 2].get('endTime'))}"

                    # Find "tomorrow"
                    if index + 3 < len(periods):
                        self.tomorrow_temperature = periods[index + 3].get(
                            "temperature"
                        )
                        self.tomorrow_forecast = periods[index + 3].get("shortForecast")
                        self.tomorrow_time_period = f"{datetime.fromisoformat(periods[index + 3].get('startTime'))} to {datetime.fromisoformat(periods[index + 3].get('endTime'))}"

                    break
                # Evening render
                else:
                    # Find "tonight"
                    self.tonight_temperature = period.get("temperature")
                    self.tonight_forecast = period.get("shortForecast")
                    self.today_time_period = f"{period_dt_start} to {period_dt_end}"

                    # Find "tomorrow"
                    if index + 1 < len(periods):
                        self.tomorrow_temperature = periods[index + 1].get(
                            "temperature"
                        )
                        self.tomorrow_forecast = periods[index + 1].get("shortForecast")
                        self.tomorrow_time_period = f"{datetime.fromisoformat(periods[index + 1].get('startTime'))} to {datetime.fromisoformat(periods[index + 1].get('endTime'))}"

                    break

            # Render during the day
            elif period_dt_start.date() == self.date:
                # Find "today"
                self.today_temperature = period.get("temperature")
                self.today_forecast = period.get("shortForecast")
                self.today_time_period = f"{period_dt_start} to {period_dt_end}"

                # Find "tonight"
                if index + 1 < len(periods):
                    self.tonight_temperature = periods[index + 1].get("temperature")
                    self.tonight_forecast = periods[index + 1].get("shortForecast")
                    self.tonight_time_period = f"{datetime.fromisoformat(periods[index + 1].get('startTime'))} to {datetime.fromisoformat(periods[index + 1].get('endTime'))}"

                # Find "tomorrow"
                if index + 2 < len(periods):
                    self.tomorrow_temperature = periods[index + 2].get("temperature")
                    self.tomorrow_forecast = periods[index + 2].get("shortForecast")
                    self.tomorrow_time_period = f"{datetime.fromisoformat(periods[index + 2].get('startTime'))} to {datetime.fromisoformat(periods[index + 2].get('endTime'))}"

                break

        # Verify all dates found
        if None in [
            self.today_temperature,
            self.today_forecast,
            self.tonight_temperature,
            self.tonight_forecast,
            self.tomorrow_temperature,
            self.tomorrow_forecast,
        ]:
            print("Warning: Not all dates found!\n")

        def get_icon_path(icon_folder, forecast, night=False):
            # Set default icon
            selected_icon = "Sun.png"

            weather_priorities = {
                "Showers And Thunderstorms": "Cloud Bolt Rain.png",
                "T-storms": "Cloud Bolt.png",
                "Rain And Snow": "Cloud Sleet.png",
                "Heavy Rain": "Cloud Heavyrain.png",
                "Rain Showers": "Cloud Sun Rain.png|Cloud Moon Rain.png",
                "Rain": "Cloud Rain.png",
                "Drizzle": "Cloud Drizzle.png",
                "Snow": "Cloud Snow.png",
                "Frost": "Snow.png",
                "Smoke": "Smoke.png",
                "Haze": "Smoke.png",
                "Fog": "Cloud Fog.png",
                "Partly Cloudy": "Cloud Sun.png|Cloud Moon.png",
                "Mostly Cloudy": "Cloud Sun.png|Cloud Moon.png",
                "Cloudy": "Cloud.png",
                "Blowing Dust": "Wind.png",
                "Partly Sunny": "Cloud Sun.png",
                "Mostly Sunny": "Cloud Sun.png",
                "Sunny": "Sun.png",
                "Clear": "Sun.png|Moon.png",
            }

            # Complex forecast
            if " then " in forecast:
                forecast_0 = forecast.split(" then ")[0]
                forecast_1 = forecast.split(" then ")[1]

                # Get prioritized forecast
                for priority, icon in weather_priorities.items():
                    if priority in forecast_0:
                        selected_forecast = forecast_0
                        selected_icon = icon
                        break
                    if priority in forecast_1:
                        selected_forecast = forecast_1
                        selected_icon = icon
                        break
            # Simple forecast
            else:
                # Get prioritized forecast
                for priority, icon in weather_priorities.items():
                    if priority in forecast:
                        selected_forecast = forecast
                        selected_icon = icon
                        break

            # Get icon name
            icon_name = (
                selected_icon.split("|")[int(night)]
                if "|" in selected_icon
                else selected_icon
            )

            return f"{icon_folder}/{icon_name}"

        # Get "today" icon path
        if not self.today_forecast is None:
            self.today_icon_path = get_icon_path(
                self.weather_icon_folder, self.today_forecast, False
            )

        # Get "tonight" icon path
        if not self.tonight_forecast is None:
            self.tonight_icon_path = get_icon_path(
                self.weather_icon_folder, self.tonight_forecast, True
            )

        # Get "tomorrow" icon path
        if not self.tomorrow_forecast is None:
            self.tomorrow_icon_path = get_icon_path(
                self.weather_icon_folder, self.tomorrow_forecast, False
            )

        print(
            f"Today: \n\ttemp: {self.today_temperature}° \n\tforecast: {self.today_forecast} \n\ticon: '{self.today_icon_path}' \n\tperiod: {self.today_time_period}"
        )
        print(
            f"Tonight: \n\ttemp: {self.tonight_temperature}° \n\tforecast: {self.tonight_forecast} \n\ticon: '{self.tonight_icon_path}' \n\tperiod: {self.tonight_time_period}"
        )
        print(
            f"Tomorrow: \n\ttemp: {self.tomorrow_temperature}° \n\tforecast: {self.tomorrow_forecast} \n\ticon: '{self.tomorrow_icon_path}' \n\tperiod: {self.tomorrow_time_period}"
        )


class Park:
    def __init__(self, date, key, icon_path, refresh=True):
        # Constants
        self.park_url = f"https://api.themeparks.wiki/v1/entity/{key}/schedule"
        self.parks_json_path = "/files/data/temp/parks.json"
        self.icon_path = icon_path

        self.refresh = refresh
        self.date = date

        # Try to get new data
        if self.refresh is True:
            self.response = requests.get(self.park_url)
            self.data = json.loads(self.response.text)

            # Check for invalid data
            if ("name" not in self.data) or ("schedule" not in self.data):
                print(f"Received invalid park data: {self.data}")
                self.refresh = False

        # Load data from file
        if self.refresh is False:
            print(f"\nLoading from json...")

            # Load from file
            if os.path.exists(self.parks_json_path):
                with open(self.parks_json_path, "r") as file:
                    self.data = json.load(file).get(key, None)

                print(f"Loaded from json: {str(self.data):.175s}...\n")
            # Not found in file
            else:
                self.data = None
                self.valid = False
        # Valid data
        else:
            # print(f"Received valid park data: {self.data}")

            # Read from file
            if os.path.exists(self.parks_json_path):
                with open(self.parks_json_path, "r") as file:
                    all_parks_data = json.load(file)
            else:
                all_parks_data = dict()
                self.data = dict()

            # Update current key
            all_parks_data[key] = self.data

            # Write back to file
            with open(self.parks_json_path, "w") as file:
                json.dump(all_parks_data, file, indent=4)

        # Parse data
        self.parse()

        self.valid = True

    def parse(self):
        self.normal_hours = None
        self.special_hours = []

        # Get park name
        self.full_park_name = self.data.get("name")
        self.park_name = self.full_park_name.replace(" Theme Park", "").replace(
            " Water Park", ""
        )

        # Get list of days
        self.schedule = self.data.get("schedule")

        # Sort schedule
        self.schedule.sort(
            key=lambda item: f"{item.get('date')}{'0' if item.get('type') == 'OPERATING' else '0'}{item.get('openingTime')}"
        )

        # Process all schedule days
        for element in self.schedule:
            day_dt = date.fromisoformat(element.get("date"))

            if self.date == day_dt:
                # Get hours
                opening_time = (
                    datetime.fromisoformat(element.get("openingTime"))
                    .strftime("%I:%M %p")
                    .lstrip("0")
                )  # "9:00 AM"
                closing_time = (
                    datetime.fromisoformat(element.get("closingTime"))
                    .strftime("%I:%M %p")
                    .lstrip("0")
                )  # "5:00 PM"
                hours = f"{opening_time} to {closing_time}"

                # Store normal hours
                if element.get("type") == "OPERATING":
                    self.normal_hours = f"{opening_time} to {closing_time}"
                # Store special hours
                else:
                    ticket_type = element.get("description").replace("Special ", "")
                    self.special_hours.append((ticket_type, hours))

        # Check if no daily info
        self.normal_hours = self.normal_hours if not self.normal_hours is None else "CLOSED"

        print(f"{self.park_name}")
        print(f"  {self.normal_hours} - {self.date}")
        for key, value in self.special_hours:
            print(f"    - {key}")
            print(f"      {value}")


class Parks:
    def __init__(self, date, refresh=True):
        # Constants
        self.parks_icon_folder = "/files/data/images/parks"
        self.date = date
        self.parks = dict()

        entities = {
            "75ea578a-adc8-4116-a54d-dccb60765ef9": "magic-kingdom",
            "47f90d2c-e191-4239-a466-5892ef59a88b": "epcot",
            "288747d1-8b4f-4a64-867e-ea7c9b27bad8": "hollywood-studios",
            "1c84a229-8862-4648-9c71-378ddd2c7693": "animal-kingdom",
            "b070cbc5-feaa-4b87-a8c1-f94cca037a18": "typhoon-lagoon",
            "ead53ea5-22e5-4095-9a83-8c29300d7c63": "blizzard-beach",
        }

        # Loop through all parks
        for key, name in entities.items():
            icon_path = f"{self.parks_icon_folder}/{name}.png"

            # Get park data
            park = Park(self.date, key, icon_path, refresh)

            # Ensure valid data
            if park.valid:
                self.parks[name] = park

        # Got all valid data
        if len(self.parks) == len(entities):
            print("\nInfo: All valid park data")
        # Got some valid
        elif 0 < len(self.parks) < len(entities):
            print("\nWarning: Some valid park data")


def logprint(input):
    print(f"\n----{'-'*len(input)}----")
    print(f"--- {input} ---")
    print(f"----{'-'*len(input)}----\n")


def generate_image(desired_date, weather, parks):
    # Function for loading fonts
    def font(style="Light", size=12):
        styles = [
            "Black",
            "BlackOblique",
            "Book",
            "BookOblique",
            "Heavy",
            "HeavyOblique",
            "Light",
            "LightOblique",
            "Medium",
            "MediumOblique",
            "Oblique",
            "Roman",
        ]
        if style not in styles:
            style = "Medium"
        font_path = f"/files/data/fonts/avenir-font/AvenirLTStd-{style}.otf"
        return ImageFont.truetype(font_path, size)

    def put_text(draw, text, position, font, color):
        draw.text(position, text, font=font, fill=color)
        t_left, _, _, t_bottom = draw.textbbox(position, text, font=font)
        new_position = (t_left, t_bottom + 10)
        return new_position

    img_width = 1920
    img_height = 1080

    small = 15
    medium = 20
    large = 22
    xlarge = 25
    xxlarge = 30

    white = (255, 255, 255)
    grey = (150, 150, 150)
    start_position = (1450, 0)
    position = (start_position[0], start_position[1])

    feather_radius = 50
    feather_opacity = 200

    icon_size = 50

    # Create image to put text and images onto
    img_text = Image.new("RGBA", (img_width, img_height), (0, 0, 0, 0))
    draw_text = ImageDraw.Draw(img_text)

    # Add current date
    formatted_date = desired_date.strftime("%A")
    position = put_text(draw_text, formatted_date, position, font(size=xxlarge), grey)
    formatted_date = desired_date.strftime("%B %d")
    position = put_text(
        draw_text, formatted_date, position, font(style="Medium", size=xxlarge), white
    )

    # Add spacing between date and weather
    position = (position[0], position[1] + 25)

    # Add weather
    if not (
        weather.today_temperature is None
        and weather.tonight_temperature is None
        and weather.tomorrow_temperature is None
    ):
        # Make copy of position
        position_today = (position[0], position[1])
        position_tonight = (position[0] + 130, position[1])
        position_tomorrow = (position[0] + 280, position[1])

        if not weather.today_icon_path is None:
            icon = Image.open(weather.today_icon_path, "r").resize(
                (icon_size, icon_size)
            )
            icon_w, icon_h = icon.size
            img_text.paste(icon, position_today, icon)
            position = (position[0], position_today[1] + icon_size)
        if not weather.tonight_icon_path is None:
            icon = Image.open(weather.tonight_icon_path, "r").resize(
                (icon_size, icon_size)
            )
            icon_w, icon_h = icon.size
            img_text.paste(icon, position_tonight, icon)
            position = (position[0], position_tonight[1] + icon_size)
        if not weather.tomorrow_icon_path is None:
            icon = Image.open(weather.tomorrow_icon_path, "r").resize(
                (icon_size, icon_size)
            )
            icon_w, icon_h = icon.size
            img_text.paste(icon, position_tomorrow, icon)
            position = (position[0], position_tomorrow[1] + icon_size)

        if not weather.today_temperature is None:
            new_position_today = put_text(
                draw_text,
                "Today",
                (position_today[0] + icon_size + 5, position_today[1]),
                font(style="Medium", size=large),
                grey,
            )
        if not weather.tonight_temperature is None:
            new_position_tonight = put_text(
                draw_text,
                "Tonight",
                (position_tonight[0] + icon_size + 5, position_tonight[1]),
                font(style="Medium", size=large),
                grey,
            )
        if not weather.tomorrow_temperature is None:
            new_position_tomorrow = put_text(
                draw_text,
                "Tomorrow",
                (position_tomorrow[0] + icon_size + 5, position_tomorrow[1]),
                font(style="Medium", size=large),
                grey,
            )

        if not weather.today_temperature is None:
            new_position_today = put_text(
                draw_text,
                str(weather.today_temperature) + "°",
                (position_today[0] + icon_size + 5, position_today[1] + large + 5),
                font(size=xxlarge),
                white,
            )
            position = (position[0], new_position_today[1])
        if not weather.tonight_temperature is None:
            new_position_tonight = put_text(
                draw_text,
                str(weather.tonight_temperature) + "°",
                (position_tonight[0] + icon_size + 5, position_tonight[1] + large + 5),
                font(size=xxlarge),
                white,
            )
            position = (position[0], new_position_tonight[1])
        if not weather.tomorrow_temperature is None:
            new_position_tomorrow = put_text(
                draw_text,
                str(weather.tomorrow_temperature) + "°",
                (
                    position_tomorrow[0] + icon_size + 5,
                    position_tomorrow[1] + large + 5,
                ),
                font(size=xxlarge),
                white,
            )
            position = (position[0], new_position_tomorrow[1])

    # Add spacing between weather and park data
    position = (position[0], position[1] + 30)

    # Add park data
    for park in parks.parks.values():
        icon = Image.open(park.icon_path, "r").resize((icon_size, icon_size))
        icon_w, icon_h = icon.size
        img_text.paste(icon, (position[0], position[1]), icon)

        # Park name
        new_position_park = put_text(
            draw_text,
            park.park_name,
            (position[0] + icon_w + 15, position[1]),
            font(style="Medium", size=xlarge),
            white,
        )

        # Park normal hours
        new_position_park = put_text(
            draw_text,
            park.normal_hours,
            (position[0] + icon_w + 15, position[1] + xlarge + 8),
            font(style="Medium", size=large),
            white,
        )

        # Add spacing before special hours
        new_position_park = (new_position_park[0], position[1] + icon_size + 20)

        # Park special hours
        for ticket_type, ticket_hours in park.special_hours:
            new_position_park = put_text(
                draw_text,
                "  " + ticket_type + ": " + ticket_hours,
                new_position_park,
                font(size=medium, style="Bold"),
                grey,
            )

        # Add spacing after special hours
        position = (position[0], new_position_park[1] + 10)

    # Create image to put fade onto
    img_feather = Image.new("RGBA", (img_width, img_height), (0, 0, 0, 0))
    draw_feather = ImageDraw.Draw(img_feather)
    draw_feather.rectangle(
        (
            start_position[0] - feather_radius,
            start_position[1] - feather_radius,
            img_width + feather_radius,
            img_height + feather_radius,
        ),
        fill=(0, 0, 0, feather_opacity),
    )
    img_feather = img_feather.filter(ImageFilter.GaussianBlur(radius=feather_radius))

    # Add layers to final image
    img = Image.new("RGBA", (img_width, img_height), (0, 0, 0, 0))
    img.paste(img_feather)
    img.paste(
        img_text, (0, (img_height // 2) - (position[1] // 2)), img_text
    )  # Center vertically

    # Write image
    image_location = "/files/data/temp/today.png"
    img.save(image_location)
    print(f"Wrote image to '{image_location}'")


def generate_video():
    (
        ffmpeg.input("/files/data/video/loop.mp4", nostdin=None)
        .overlay(ffmpeg.input("/files/data/temp/today.png"))
        .output(
            "/files/data/temp/upcoming.mp4",
            y=None,
            g=60,
            **{"b:v": "7000k"},
            hide_banner=None,
            loglevel="warning",
            progress="-",
        )
        .run()
    )


def load_video():
    from_location = "/files/data/temp/upcoming.mp4"
    to_location = "/files/data/temp/today.mp4"

    print(f"Moving from '{from_location}' to '{to_location}'")

    os.rename(from_location, to_location)


def validate(date_string):
    try:
        return date.fromisoformat(date_string)
    except ValueError:
        raise argparse.ArgumentTypeError(
            f"Invalid date format: '{date_string}'. Expected format is 'YYYY-MM-DD'"
        )


def main():
    # Create argparser
    parser = argparse.ArgumentParser(
        description="A tool for updating the weather and park data in a looping video for livestreaming."
    )

    data_group = parser.add_mutually_exclusive_group()
    data_group.add_argument("-d", "--data", action="store_const", const=True, dest="data", help="Get the latest park and weather data")
    data_group.add_argument("-nd", "--no-data", action="store_const", const=False, dest="data", help="Don't get the latest park and weather data")

    image_group = parser.add_mutually_exclusive_group()
    image_group.add_argument("-i", "--image", action="store_const", const=True, dest="image", help="Render the overlay image")
    image_group.add_argument("-ni", "--no-image", action="store_const", const=False, dest="image", help="Don't render the overlay image")

    video_group = parser.add_mutually_exclusive_group()
    video_group.add_argument("-v", "--video", action="store_const", const=True, dest="video", help="Render the looping video")
    video_group.add_argument("-nv", "--no-video", action="store_const", const=False, dest="video", help="Don't render the looping video")

    load_group = parser.add_mutually_exclusive_group()
    load_group.add_argument("-l", "--load", action="store_const", const=True, dest="load", help="Load the latest looping video")
    load_group.add_argument("-nl", "--no-load", action="store_const", const=False, dest="load", help="Don't load the latest looping video")

    simple_group = parser.add_mutually_exclusive_group()
    simple_group.add_argument("--only-data", action="store_const", const="data", dest="only", help="Only get data and exit (equivalent to `--data --no-image --no-video --no-load`)")
    simple_group.add_argument("--only-image", action="store_const", const="image", dest="only", help="Only get data, generate image, and exit (equivalent to `--data --image --no-video --no-load`)")
    simple_group.add_argument("--only-video", action="store_const", const="video", dest="only", help="Only get data, generate image, generate video, and exit (equivalent to `--data --image --video --no-load`)")

    date_group = parser.add_mutually_exclusive_group()
    date_group.add_argument("--date", type=validate, default=date.today() ,help="Select custom date for processing")
    date_group.add_argument("--tomorrow", action="store_true", default=None, help="Use next day for processing")

    # Parse the arguments
    args = parser.parse_args()

    # Process arguments into a configuration
    config = {
        "data": (not args.data is False) and args.only in ["data", "image", "video", None],
        "image": (not args.image is False) and args.only in ["image", "video", None],
        "video": (not args.video is False) and args.only in ["video", None],
        "load": (not args.load is False) and args.only in [None],
    }

    # Process desired date
    config["desired_date"] = (
        date.today() + timedelta(days=1) if args.tomorrow else args.date
    )

    logprint(f"{"-"*20}")

    logprint(f"Configuration:")
    print(f"start time: {datetime.now()}")
    print(f"data: {config.get('data')}")
    print(f"image: {config.get('image')}")
    print(f"video: {config.get('video')}")
    print(f"load: {config.get('load')}")
    print(f"selected-date: {config.get('desired_date')}")

    # Get new parks and weather data
    if config.get("data"):
        logprint("Getting new weather data")
        weather = Weather(config.get("desired_date"))

        logprint("Getting new parks data")
        parks = Parks(config.get("desired_date"))
    else:
        logprint("Loading cached weather data")
        weather = Weather(config.get("desired_date"), False)

        logprint("Loading cached parks data")
        parks = Parks(config.get("desired_date"), False)

    if config.get("image"):
        logprint("Generating image")
        generate_image(config.get("desired_date"), weather, parks)

    if config.get("video"):
        logprint("Generating video")
        generate_video()

    if config.get("load"):
        logprint("Loading video")
        load_video()

    logprint("Done!")


if __name__ == "__main__":
    main()
