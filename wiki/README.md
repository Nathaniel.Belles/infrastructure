# wiki

## Instructions
1. Download the Wikipedia backup into the `./data` folder: `wget --output-document ./data/wikipedia_en_simple_all_maxi_2024-06.zim https://download.kiwix.org/zim/wikipedia/wikipedia_en_simple_all_maxi_2024-06.zim`
2. Update the command in the `compose.yml` to match the downloaded backup
